<%@page import="javax.ws.rs.core.Response" %>
<%@page import="javax.ws.rs.client.ClientBuilder" %>
<%@page import="javax.ws.rs.client.Client" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    String miip = request.getHeader("X-Forwarded-For");
    String t = miip;
    if(miip == null){
        miip = request.getRemoteAddr();
    }
    String url = "https://api.ip2country.info/ip?" + miip;

    Client client = ClientBuilder.newClient();
    Response r = client.target(url).request().get();
%>
<head>
    <title>JSP Page</title>
</head>
<body>
<h1>Mi IP:</h1><%= miip%>
<br>
<%= r.readEntity(String.class)%><br>
</body>
</html>
